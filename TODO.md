# TODO
* Additional resource checks: symlink detection and per-file path validation.
* Streaming writes, instead of loading the file in memory.
* Additional mime-types file. (/etc/mime.types)
